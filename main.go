package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

type tweet struct {
	usuario string
	hora    string
	fecha   string
	texto   string
}

func getClient(creds *Credentials) (*twitter.Client, error) {

	config := oauth1.NewConfig(creds.ConsumerKey, creds.ConsumerSecret)

	token := oauth1.NewToken(creds.AccessToken, creds.AccessTokenSecret)

	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

	verifyParams := &twitter.AccountVerifyParams{
		SkipStatus:   twitter.Bool(true),
		IncludeEmail: twitter.Bool(true),
	}

	user, _, err := client.Accounts.VerifyCredentials(verifyParams)
	if err != nil {
		return nil, err
	}

	fmt.Printf("User's ACCOUNT:\n%+v\n", user.UtcOffset)

	return client, nil
}

func main() {

	fmt.Println("Go-Twitter Bot v0.01")

	item1()
	item2()
	item3()
	item4()
	item5()

}

func item1() {

	client, err := getClient(&creds)

	if err != nil {
		log.Println("Error getting Twitter Client")
		log.Println(err)
	}

	search, resp, err := client.Search.Tweets(&twitter.SearchTweetParams{
		Query: "billetaje electronico",
		Count: 100,
	})

	if err != nil {
		log.Print(err)
	}

	log.Printf("%+v\n", resp.Status)
	log.Println("---------------------------------------")
	log.Printf("%+v\n", len(search.Statuses))

	generarArchivo(search.Statuses)

	for i := 0; i < len(search.Statuses); i++ {
		log.Printf("%+v\n", search.Statuses[i].Text)
		log.Printf("%+v\n", search.Statuses[i].CreatedAt)
		log.Println("---------------------------------------")
	}

}

func item5() {
	csvFile, err := os.Create("item4.csv")

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	client, err := getClient(&creds)

	if err != nil {
		log.Println("Error getting Twitter Client")
		log.Println(err)
	}

	search, resp, err := client.Search.Tweets(&twitter.SearchTweetParams{
		Query: "#sorteo",
		Count: 50,
	})

	log.Printf("%+v\n", resp.Status)

	if err != nil {
		log.Print(err)
	}

	var longitud = len(search.Statuses)
	matriz := [][]string{}

	for i := 0; i < longitud; i++ {

		var texto string

		texto = search.Statuses[i].Text
		usuario := search.Statuses[i].User.ScreenName

		row1 := []string{usuario, texto}

		matriz = append(matriz, row1)

	}

	csvwriter := csv.NewWriter(csvFile)

	for i := 0; i < longitud; i++ {
		csvwriter.Write(matriz[i])
	}
	csvwriter.Flush()
	csvFile.Close()

	var usuarioGanador int
	var usuarioSuplente int
	var index int
	cont := 0

	for cont < 2 {
		if cont == 0 {
			index = rand.Int() % len(matriz)
			usuarioGanador = index
			cont++
		}
		index = rand.Int() % len(matriz)
		if usuarioGanador == index {
			index = rand.Int() % len(matriz)
		} else {
			usuarioSuplente = index
			cont++
		}
	}

	fmt.Printf("Usuario ganador: %s\n", matriz[usuarioGanador][0])
	fmt.Printf("Usuario suplente: %s\n", matriz[usuarioSuplente][0])
}

func item4() {
	client, err := getClient(&creds)

	if err != nil {
		log.Println("Error getting Twitter Client")
		log.Println(err)
	}

	csvFile, err := os.Create("item3.csv")

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	csvwriter := csv.NewWriter(csvFile)

	demux := twitter.NewSwitchDemux()

	demux.Tweet = func(tweet *twitter.Tweet) {

		fmt.Printf("%s: %s\n", tweet.User.ScreenName, tweet.Text)
		fecha, err := tweet.CreatedAtTime()

		if err != nil {
			log.Print(err)
		}

		texto := tweet.Text
		usuario := tweet.User.ScreenName
		hora := fecha.Format("3:04PM")
		dia := fecha.Format("2006-01-02")

		row1 := []string{usuario, hora, dia, texto}

		csvwriter.Write(row1)
	}

	fmt.Println("Starting Stream...")

	filterParams := &twitter.StreamFilterParams{
		Track:         []string{"#cryptocurrencynews"},
		StallWarnings: twitter.Bool(true),
	}
	stream, err := client.Streams.Filter(filterParams)
	if err != nil {
		log.Fatal(err)
	}

	go demux.HandleChan(stream.Messages)

	// Wait for SIGINT and SIGTERM (HIT CTRL-C)
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	log.Println(<-ch)

	fmt.Println("Stopping Stream...")
	stream.Stop()

	csvwriter.Flush()
	csvFile.Close()

}

func item3() {

	csvFile, _ := os.Open("item2.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var tweets []tweet
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		tweets = append(tweets, tweet{
			usuario: line[0],
			hora:    line[1],
			fecha:   line[2],
			texto:   line[3],
		})
	}

	for i := range tweets {
		ocurrencias := contarOcurrencias(tweets, tweets[i].usuario)
		fmt.Printf("Ocurrencias del usuario %s: %d\n", tweets[i].usuario, ocurrencias)
	}

}

func contarOcurrencias(tweets []tweet, usuario string) (cantidad int) {

	cantidad = 0
	for i := range tweets {
		if usuario == tweets[i].usuario {
			cantidad++
		}
	}
	return cantidad

}

func item2() {

	csvFile, _ := os.Open("item1.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var tweets []tweet
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		tweets = append(tweets, tweet{
			usuario: line[0],
			hora:    line[1],
			fecha:   line[2],
			texto:   line[3],
		})
	}

	quicksort(tweets)

	csvFile, err := os.Create("item2.csv")

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	matriz := [][]string{} //crear slice

	for i := 0; i < len(tweets); i++ {

		usuario := tweets[i].usuario
		hora := tweets[i].hora
		dia := tweets[i].fecha
		texto := tweets[i].texto

		row1 := []string{usuario, hora, dia, texto}

		matriz = append(matriz, row1)

	}

	csvwriter := csv.NewWriter(csvFile)

	for i := 0; i < len(matriz); i++ {
		csvwriter.Write(matriz[i])
	}
	csvwriter.Flush()
	csvFile.Close()

	fmt.Println(tweets)

}

func quicksort(a []tweet) []tweet {
	if len(a) < 2 {
		return a
	}

	left, right := 0, len(a)-1

	pivot := rand.Int() % len(a)

	a[pivot], a[right] = a[right], a[pivot]

	for i := range a {
		if a[i].usuario < a[right].usuario {
			a[left], a[i] = a[i], a[left]
			left++
		}
	}

	a[left], a[right] = a[right], a[left]

	quicksort(a[:left])
	quicksort(a[left+1:])

	return a
}

func generarArchivo(listaTweets []twitter.Tweet) {

	csvFile, err := os.Create("item1.csv")

	var longitud = len(listaTweets)

	log.Printf("%+v\n", len(listaTweets))

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	matriz := [][]string{} //crear slice

	for i := 0; i < longitud; i++ {

		fecha, err := listaTweets[i].CreatedAtTime()

		if err != nil {
			log.Print(err)
		}

		var texto string

		if strings.Contains(listaTweets[i].Text, "RT") {
			texto = listaTweets[i].RetweetedStatus.Text
		} else {
			texto = listaTweets[i].Text
		}

		usuario := listaTweets[i].User.ScreenName
		hora := fecha.Format("3:04PM")
		dia := fecha.Format("2006-01-02")

		row1 := []string{usuario, hora, dia, texto}

		matriz = append(matriz, row1)

	}

	csvwriter := csv.NewWriter(csvFile)

	for i := 0; i < longitud; i++ {
		csvwriter.Write(matriz[i])
	}
	csvwriter.Flush()
	csvFile.Close()
}
